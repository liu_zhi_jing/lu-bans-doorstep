package cn.tedu.luban.order.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LubanOrderWebapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LubanOrderWebapiApplication.class, args);
    }

}
