package cn.tedu.luban.order.webapi.controller;


import cn.tedu.luban.commons.exception.LubanServiceException;
import cn.tedu.luban.commons.pojo.order.dto.*;
import cn.tedu.luban.commons.pojo.order.vo.OrderDetailVO;
import cn.tedu.luban.commons.pojo.order.vo.OrderVO;
import cn.tedu.luban.commons.restful.JsonPage;
import cn.tedu.luban.commons.restful.Result;
import cn.tedu.luban.order.service.IOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(value = "order", tags = "订单")
@Slf4j
@RestController
@RequestMapping("order")
public class OrderController {


    @Autowired
    private IOrderService orderService;

    @GetMapping(value = "info")
    @ApiOperation("订单详情")
    public Result<OrderDetailVO> getOrder(String orderNo) throws LubanServiceException  {
        OrderDetailVO orderDetailVO = orderService.getOrderByOrderNo(orderNo);
        return new Result<>(orderDetailVO);
    }

    @PostMapping(value = "list")
    @ApiOperation("订单列表")
    public Result<JsonPage<OrderVO>> list(OrderListDTO orderListDTO,
                                            Integer pageNo,Integer pageSize)
                                            throws LubanServiceException {
        JsonPage<OrderVO> list = orderService.list(orderListDTO,pageNo,pageSize);
        return new Result<>(list);
    }

    @PostMapping(value = "sign")
    @ApiOperation("签到")
    public Result<String> sign(OrderSignDTO orderSignDTO) throws LubanServiceException {
        return new Result<>(orderService.sign(orderSignDTO));
    }

    @PostMapping(value = "create")
    @ApiOperation("抢单")
    public String create(OrderDTO orderDTO) throws LubanServiceException {
        return orderService.create(orderDTO);
    }



    @PostMapping(value = "confirm")
    @ApiOperation("上传服务图片")
    public String confirm(OrderConfirmDTO orderConfirmDTO) throws LubanServiceException  {
        return orderService.confirm(orderConfirmDTO);
    }

    @PostMapping(value = "finish")
    @ApiOperation("完成订单")
    public String finish(OrderFinishDTO orderFinishDTO) throws LubanServiceException {
        return orderService.finish(orderFinishDTO);
    }

}













