package cn.tedu.luban.order.webapi.mapper;

import cn.tedu.luban.commons.pojo.order.model.OrderLog;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderLogMapper {

    List<OrderLog> getOrderLogByOrderNo(String orderNo);

    Long insert(OrderLog orderLog);
}
