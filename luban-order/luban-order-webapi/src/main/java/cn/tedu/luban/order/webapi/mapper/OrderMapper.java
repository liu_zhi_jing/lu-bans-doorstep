package cn.tedu.luban.order.webapi.mapper;

import cn.tedu.luban.commons.pojo.order.dto.OrderDTO;
import cn.tedu.luban.commons.pojo.order.dto.OrderListDTO;
import cn.tedu.luban.commons.pojo.order.model.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderMapper {

    Order getOrderByOrderNo(String orderNo);

    List<Order> queryOrders(OrderListDTO orderListDTO);

    Integer update(Order order);

    Long insert(Order order);
}
