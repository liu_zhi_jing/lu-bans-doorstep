package cn.tedu.luban.order.service;

import cn.tedu.luban.commons.pojo.order.dto.*;
import cn.tedu.luban.commons.pojo.order.vo.OrderDetailVO;
import cn.tedu.luban.commons.pojo.order.vo.OrderVO;
import cn.tedu.luban.commons.restful.JsonPage;

public interface IOrderService {

    OrderDetailVO getOrderByOrderNo(String orderNo);

    JsonPage<OrderVO> list(OrderListDTO orderListDTO,Integer pageNo,Integer pageSize);

    String sign(OrderSignDTO orderSignDTO);

    String create(OrderDTO orderDTO);

    String confirm(OrderConfirmDTO orderConfirmDTO);

    String finish(OrderFinishDTO orderFinishDTO);

    void finishOrder(String orderNo, Long workerId);
}
