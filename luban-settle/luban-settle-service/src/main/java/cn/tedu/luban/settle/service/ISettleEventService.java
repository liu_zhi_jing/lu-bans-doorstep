package cn.tedu.luban.settle.service;

import cn.tedu.luban.commons.pojo.order.dto.OrderMqDTO;

public interface ISettleEventService {

    void sendSettledEvent(OrderMqDTO orderMq);

}
