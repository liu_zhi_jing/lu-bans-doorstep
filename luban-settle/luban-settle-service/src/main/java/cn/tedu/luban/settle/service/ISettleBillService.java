package cn.tedu.luban.settle.service;

import cn.tedu.luban.commons.pojo.order.dto.OrderMqDTO;

public interface ISettleBillService {
    void settle(OrderMqDTO settleDTO);

    void mockPayAndUpdateSettleBillStatus(OrderMqDTO orderMqDTO);
}
