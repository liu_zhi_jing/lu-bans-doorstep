package cn.tedu.luban.settle.webapi.service.impl;

import cn.tedu.luban.account.service.IAccountService;
import cn.tedu.luban.commons.enums.settle.SettleStatusEnum;
import cn.tedu.luban.commons.enums.settle.SettleTypeEnum;
import cn.tedu.luban.commons.pojo.account.dto.PaymentDTO;
import cn.tedu.luban.commons.pojo.order.dto.OrderMqDTO;
import cn.tedu.luban.commons.pojo.settle.dto.SettleBillDTO;
import cn.tedu.luban.commons.pojo.settle.model.SettleBill;
import cn.tedu.luban.settle.service.ISettleBillService;
import cn.tedu.luban.settle.service.ISettleEventService;
import cn.tedu.luban.settle.webapi.mapper.SettleMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SettleBillServiceImpl implements ISettleBillService {


    @DubboReference
    private IAccountService dubboAccountService;

    @Autowired
    private ISettleEventService settleEventService;

    @Autowired
    private SettleMapper settleMapper;

    @Override
    public void settle(OrderMqDTO settleDTO) {
        // 根据orderNo查询结算记录
        List<SettleBill> settleBills=settleMapper.getSettleBillByOrderNo(settleDTO.getOrderNo());
        // 如果结算记录数等于0表示没有结算
        if(settleBills.size()==0){
            //通过需求单号查询供应商信息 包括基本信息和分润信息
            List<SettleBillDTO> settleBillDTOs=buildSettleBillParam(settleDTO);
            List<SettleBill> addSettleBills=new ArrayList<>();
            for (SettleBillDTO settleBillDTO: settleBillDTOs ){
                SettleBill settleBill=new SettleBill();
                BeanUtils.copyProperties(settleBillDTO, settleBill);
                settleBill.setRequestOrderPrice(settleBillDTO.getRequestOrderRawPrice());
                settleBill.setGmtCreate(System.currentTimeMillis());
                settleBill.setGmtModified(System.currentTimeMillis());
                settleBill.setCreateUserId(99L);
                settleBill.setModifiedUserId(99L);
                settleBill.setCreateUserName("mock1");
                settleBill.setModifiedUserName("mock1");
                addSettleBills.add(settleBill);
                System.out.println(settleBill);
            }
            settleMapper.batchInsert(addSettleBills);
            settleEventService.sendSettledEvent(settleDTO);

        }

    }

    @Override
    public void mockPayAndUpdateSettleBillStatus(OrderMqDTO orderMqDTO) {
        PaymentDTO paymentDTO = new PaymentDTO();
        paymentDTO.setUserId(orderMqDTO.getWorkerId());
        paymentDTO.setOrderNo(orderMqDTO.getOrderNo());
        paymentDTO.setTotalAmount(orderMqDTO.getRequestOrderPrice());
        log.info("pay req:{}",paymentDTO);
        Long result = dubboAccountService.mockPayment(paymentDTO);
        if (result > 0) {
            log.info("pay success req:{},resp:{}",paymentDTO,result);
            SettleBill settleBill = new SettleBill();

            settleBill.setOrderNo(orderMqDTO.getOrderNo());
            settleBill.setStatus(SettleStatusEnum.SETTLE_STATUS_STEELED.getStatus());
            settleBill.setPaymentTime(System.currentTimeMillis());
            settleBill.setGmtModified(System.currentTimeMillis());
            settleBill.setModifiedUserId(99L);
            settleBill.setModifiedUserName("mock");
            settleMapper.updateStatus(settleBill);
        }else {
            log.info("pay failed req:{},resp:{}",paymentDTO,result);
        }

    }

    private List<SettleBillDTO> buildSettleBillParam(OrderMqDTO settleDTO) {
        String requestOrderNo = settleDTO.getRequestOrderNo();
        List<SettleBillDTO> settleBillDTOs = new ArrayList<>();
        //展示价格
        Long requestOrderPrice = settleDTO.getRequestOrderPrice();
        //原始价格
        Long requestOrderRawPrice = settleDTO.getRequestOrderRawPrice();
        // 工人结算信息
        SettleBillDTO worderSettleBillDTO  = buildSettleBillParam(settleDTO,requestOrderPrice,
                SettleTypeEnum.SETTLE_TYPE_WORKER.getStatus(), settleDTO.getWorkerId());
        // 平台结算信息
        SettleBillDTO platformSettleBillDTO  = buildSettleBillParam(settleDTO,
                getPlatformAmount(requestOrderRawPrice,requestOrderPrice),
                SettleTypeEnum.SETTLE_TYPE_PLATFORM.getStatus(),999L);
        settleBillDTOs.add(platformSettleBillDTO);
        settleBillDTOs.add(worderSettleBillDTO);
        return settleBillDTOs;
    }

    private SettleBillDTO buildSettleBillParam(OrderMqDTO settleDTO, Long amount, Integer type, Long userId) {
        SettleBillDTO settleBillDTO = new SettleBillDTO();
        BeanUtils.copyProperties(settleDTO,settleBillDTO);
        settleBillDTO.setAmount(amount);
        settleBillDTO.setType(type);
        settleBillDTO.setUserId(userId);
        settleBillDTO.setScale(settleDTO.getProfitScale());
        settleBillDTO.setRequestUserId(settleDTO.getUserId());
        settleBillDTO.setRequestUserName(settleDTO.getUserName());
        settleBillDTO.setRequestUserPhone(settleDTO.getUserPhone());
        settleBillDTO.setStatus(SettleStatusEnum.SETTLE_STATUS_WAIT.getStatus());
        settleBillDTO.setOrderGmtCreate(settleDTO.getGmtCreate());
        settleBillDTO.setTotalAmount(amount);
        return settleBillDTO;


    }

    private Long getPlatformAmount(Long rawPrice,Long workerPrice){
        return new BigDecimal(rawPrice).subtract(new BigDecimal(workerPrice)).longValue();
    }
}













