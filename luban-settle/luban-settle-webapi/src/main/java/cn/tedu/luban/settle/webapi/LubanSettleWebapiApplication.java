package cn.tedu.luban.settle.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LubanSettleWebapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LubanSettleWebapiApplication.class, args);
    }

}
