package cn.tedu.luban.settle.webapi.mapper;

import cn.tedu.luban.commons.pojo.settle.model.SettleBill;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SettleMapper {

    void batchInsert(@Param("settleBills") List<SettleBill> settleBills);

    void updateStatus(SettleBill settleBill);

    List<SettleBill> getSettleBillByOrderNo(String orderNo);
}
