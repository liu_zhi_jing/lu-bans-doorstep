package cn.tedu.luban.worker.webapi.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

// 当前类是配置Spring扫描环境的配置类,必须添加注解@Configuration才能生效
@Configuration
// 扫描统一异常处理类所在的包,也就是commons模块中的包,让异常处理类生效
@ComponentScan({"cn.tedu.luban.commons.exception.handler",
                "cn.tedu.luban.commons.utils"})
public class CommonsConfiguration {
}







