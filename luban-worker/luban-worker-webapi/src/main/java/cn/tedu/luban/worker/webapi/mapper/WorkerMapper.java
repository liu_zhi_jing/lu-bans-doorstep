package cn.tedu.luban.worker.webapi.mapper;

import cn.tedu.luban.commons.pojo.worker.model.Worker;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkerMapper {

    Worker getWorkerByWorkerId(Long userId);

    int delete(Long  userId);

    int insert(Worker worker);
}

