package cn.tedu.luban.worker.webapi.mapper;

import cn.tedu.luban.commons.pojo.worker.model.WorkerArea;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkerAreaMapper {


    List<WorkerArea> getAreaByWorkerId(Long userId);

    int delete(Long userId);

    int insert(WorkerArea workerArea);

}
