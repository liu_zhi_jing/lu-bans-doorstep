package cn.tedu.luban.worker.webapi.mapper;

import org.springframework.stereotype.Repository;

@Repository
public interface AuditMapper {

    String getRejectReasonByUserId(Long workerId);

}
