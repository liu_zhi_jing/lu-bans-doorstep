package cn.tedu.luban.worker.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LubanWorkerWebapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LubanWorkerWebapiApplication.class, args);
    }

}
