package cn.tedu.luban.worker.webapi.controller;

import cn.tedu.luban.commons.pojo.worker.dto.WorkerCreateDTO;
import cn.tedu.luban.commons.pojo.worker.vo.WorkerVO;
import cn.tedu.luban.commons.restful.Result;
import cn.tedu.luban.commons.utils.Base64Util;
import cn.tedu.luban.commons.utils.ByteStreamFromURL;
import cn.tedu.luban.commons.utils.HttpUtil;
import cn.tedu.luban.worker.service.IWorkerService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;

@RestController
@RequestMapping("/worker")
@Api(value = "worker", tags = "师傅接口")
@Slf4j
public class WorkerController {

    @Autowired
    private IWorkerService workerService;

    @Value("${baidu.card.url}")
    private String cardUrl;

    @Value("${baidu.card.token}")
    private String cartToken;

    @GetMapping(value = "detail")
    @ApiOperation(value = "师傅详情")
    public Result<WorkerVO> workerDetail() {
        WorkerVO workerVO = workerService.getWorkerDetail();
        return new Result<>(workerVO);
    }

    @PostMapping(value = "create")
    @ApiOperation(value = "师傅入驻")
    public Long create(WorkerCreateDTO workerCreateDTO) throws IOException {

        return workerService.createWorker(workerCreateDTO);
    }
}
