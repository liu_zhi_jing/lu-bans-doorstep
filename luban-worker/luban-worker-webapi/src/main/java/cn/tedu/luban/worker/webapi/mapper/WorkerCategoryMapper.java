package cn.tedu.luban.worker.webapi.mapper;

import cn.tedu.luban.commons.pojo.worker.model.WorkerArea;
import cn.tedu.luban.commons.pojo.worker.model.WorkerCategory;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface WorkerCategoryMapper {

    List<WorkerCategory> getWorkerCategoryByWorkerId(Long userId);

    int delete(Long userId);

    int insert(WorkerCategory workerCategory);
}
