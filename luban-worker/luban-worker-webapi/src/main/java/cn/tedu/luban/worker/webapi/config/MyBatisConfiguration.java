package cn.tedu.luban.worker.webapi.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
// MyBatis框架要求扫描指定的包,包中的接口会生成实现类
@MapperScan("cn.tedu.luban.worker.webapi.mapper")
public class MyBatisConfiguration {
}



