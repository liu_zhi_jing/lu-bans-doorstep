package cn.tedu.luban.worker.service;

import cn.tedu.luban.commons.pojo.worker.dto.WorkerCreateDTO;
import cn.tedu.luban.commons.pojo.worker.vo.WorkerVO;

public interface IWorkerService {


    Long createWorker(WorkerCreateDTO workerCreateDTO);
    WorkerVO getWorkerDetail();
}
