package cn.tedu.luban.sso.mapper.user;

import cn.tedu.luban.commons.pojo.ums.model.UserLoginLog;


public interface UserLoginLogMapper {
    void insertUserLoginLog(UserLoginLog userLoginLog);
}
