package cn.tedu.luban.sso.security.service.user;

import cn.tedu.luban.commons.exception.LubanServiceException;
import cn.tedu.luban.sso.pojo.dto.UserLoginDTO;

;


public interface IUserSSOService {
    String doLogin(UserLoginDTO userLoginDTO) throws LubanServiceException;

    void doLogout(String token) throws LubanServiceException;
}
