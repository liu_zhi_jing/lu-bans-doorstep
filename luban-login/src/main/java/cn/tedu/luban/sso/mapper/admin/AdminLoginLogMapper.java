package cn.tedu.luban.sso.mapper.admin;


import cn.tedu.luban.commons.pojo.admin.model.AdminLoginLog;

public interface AdminLoginLogMapper {
    /**
     * 记录登录admin日志
     * @param adminLoginLog
     */
    void insertAdminLoginLog(AdminLoginLog adminLoginLog);
}
