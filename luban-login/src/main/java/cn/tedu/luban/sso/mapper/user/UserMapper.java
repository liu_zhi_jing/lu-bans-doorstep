package cn.tedu.luban.sso.mapper.user;


import cn.tedu.luban.commons.pojo.ums.model.User;
import cn.tedu.luban.commons.pojo.ums.vo.UserVO;
import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("db2SqlSessionTemplate")
public interface UserMapper {
    User findByUsername(String username);

    UserVO findById(Long id);
}
