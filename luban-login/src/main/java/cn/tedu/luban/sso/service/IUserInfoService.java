package cn.tedu.luban.sso.service;


import cn.tedu.luban.sso.pojo.vo.UserInfoVO;

public interface IUserInfoService {
    UserInfoVO userInfo(String authToken);
}
