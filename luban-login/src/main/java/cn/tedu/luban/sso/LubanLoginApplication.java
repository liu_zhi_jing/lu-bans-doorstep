package cn.tedu.luban.sso;

import cn.tedu.luban.commons.config.LubanCommonConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@Import({LubanCommonConfiguration.class})
public class LubanLoginApplication {

    public static void main(String[] args) {
        SpringApplication.run(LubanLoginApplication.class, args);
    }

}
