package cn.tedu.luban.sso.service.impl;


import cn.tedu.luban.commons.exception.LubanServiceException;
import cn.tedu.luban.commons.pojo.admin.vo.AdminVO;
import cn.tedu.luban.commons.pojo.domain.UserAuthenticationInfo;
import cn.tedu.luban.commons.pojo.ums.vo.UserVO;
import cn.tedu.luban.commons.restful.ResponseCode;
import cn.tedu.luban.commons.utils.JwtTokenUtils;

import cn.tedu.luban.sso.mapper.admin.AdminMapper;
import cn.tedu.luban.sso.mapper.user.UserMapper;
import cn.tedu.luban.sso.pojo.vo.UserInfoVO;
import cn.tedu.luban.sso.service.IUserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserInfoServiceImpl implements IUserInfoService {
    @Autowired
    private JwtTokenUtils jwtTokenUtils;
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private UserMapper userMapper;

    private static final String jwtTokenHead ="Bearer";
    @Override
    public UserInfoVO userInfo(String authToken) {
        String token=authToken.substring(jwtTokenHead.length());
        log.info("获取token:{}",token);
        UserAuthenticationInfo userInfo = jwtTokenUtils.getUserInfo(token);
        String type=userInfo.getUserType();
        Long id=userInfo.getId();
        //准备返回数据UserInfoVO
        UserInfoVO userInfoVO=new UserInfoVO();
        if (type!=null&&"ADMIN".equals(type)){
            AdminVO adminVO=adminMapper.findById(id);
            userInfoVO.setUserId(id);
            userInfoVO.setNickname(adminVO.getNickname());
            userInfoVO.setPhone(adminVO.getPhone());
            userInfoVO.setUsername(adminVO.getUsername());
        }else if(type!=null&&"USER".equals(type)){
            UserVO userVO=userMapper.findById(id);
            userInfoVO.setUserId(id);
            userInfoVO.setNickname(userVO.getNickname());
            userInfoVO.setPhone(userVO.getPhone());
            userInfoVO.setUsername(userVO.getUsername());
        }else{
            throw new LubanServiceException(ResponseCode.BAD_REQUEST,"无法获取用户信息");
        }
        return userInfoVO;
    }
}
