package cn.tedu.luban.demand.service;

import cn.tedu.luban.commons.pojo.demand.dto.RequestOrderDTO;
import cn.tedu.luban.commons.pojo.demand.dto.RequestOrderListDTO;
import cn.tedu.luban.commons.pojo.demand.vo.RequestOrderListItemVO;
import cn.tedu.luban.commons.pojo.demand.vo.RequestOrderVO;
import cn.tedu.luban.commons.restful.JsonPage;

public interface IRequestOrderService {

    public Long saveRequestOrder(RequestOrderDTO requestOrderParam);

    JsonPage<RequestOrderListItemVO> queryRequestOrderList(RequestOrderListDTO requestOrderListDTO, Integer pageNo, Integer pageSize);

    RequestOrderVO getRequestOrder(String requestOrderNo);

    Object grabOrder(String requestOrderId);

    Object cancelGrabOrder(String requestOrderId);
}
