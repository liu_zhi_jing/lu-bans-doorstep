package cn.tedu.luban.demand.webapi.mapper;

import cn.tedu.luban.commons.pojo.demand.dto.RequestOrderListDTO;
import cn.tedu.luban.commons.pojo.demand.model.RequestOrder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestOrderMapper {


    // 新增需求单
    int insert(RequestOrder requestOrder);

    // 分页查询需求单
    List<RequestOrder> queryRequestOrders(RequestOrderListDTO requestOrderListDTO);


    // 查询需求单详情
    RequestOrder getRequestOrderByOrderNo(String requestOrderNo);

    // 抢单
    Integer grabOrder(String requestOrderNo);

    // 取消抢单
    Integer cancelGrabOrder(String requestOrderNo);
}
