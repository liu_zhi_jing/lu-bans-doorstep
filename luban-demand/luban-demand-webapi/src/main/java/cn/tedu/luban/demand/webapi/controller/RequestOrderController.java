package cn.tedu.luban.demand.webapi.controller;


import cn.tedu.luban.commons.exception.LubanServiceException;
import cn.tedu.luban.commons.pojo.demand.dto.RequestOrderDTO;
import cn.tedu.luban.commons.pojo.demand.dto.RequestOrderListDTO;
import cn.tedu.luban.commons.pojo.demand.vo.RequestOrderListItemVO;
import cn.tedu.luban.commons.pojo.demand.vo.RequestOrderVO;
import cn.tedu.luban.commons.restful.JsonPage;
import cn.tedu.luban.commons.restful.Result;
import cn.tedu.luban.demand.service.IRequestOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(value = "requestOrder", tags = "需求订单接口")
@RestController
@RequestMapping("/demand/order")
public class RequestOrderController {

    @Resource
    private IRequestOrderService requestOrderService;

    @PostMapping(value = "/save")
    @ApiOperation("需求单创建")
    public Long saveDemandOrder(RequestOrderDTO requestOrderDTO) throws LubanServiceException {
        return this.requestOrderService.saveRequestOrder(requestOrderDTO);
    }


    @PostMapping(value = "search")
    @ApiOperation("需求订单列表")
    public Result<JsonPage<RequestOrderListItemVO>> queryDemandOrder(
                                        RequestOrderListDTO requestOrderListDTO,
                                        Integer pageNo,Integer pageSize) {
        JsonPage<RequestOrderListItemVO> list=
                requestOrderService.queryRequestOrderList(requestOrderListDTO,pageNo,pageSize);
        return new Result<>(list);
    }

    @GetMapping(value = "info")
    @ApiOperation("详情")
    public Result<RequestOrderVO> getRequestOrder( String requestOrderNo) throws LubanServiceException {
        RequestOrderVO requestOrderVO = requestOrderService.getRequestOrder(requestOrderNo);
        return new Result<>(requestOrderVO);
    }

    @PostMapping(value = "grab")
    @ApiOperation("抢单")
    public Result<Integer> grab(String requestOrderId) {
        return new Result(requestOrderService.grabOrder(requestOrderId));
    }

    @PostMapping(value = "cancel")
    @ApiOperation("取消抢单")
    public Result<Integer> cancelGrab(String requestOrderId) {
        return new Result(requestOrderService.cancelGrabOrder(requestOrderId));
    }



}
