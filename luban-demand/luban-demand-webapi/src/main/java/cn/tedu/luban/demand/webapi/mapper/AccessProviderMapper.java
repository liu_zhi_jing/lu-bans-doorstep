package cn.tedu.luban.demand.webapi.mapper;

import cn.tedu.luban.commons.pojo.demand.model.AccessProvider;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessProviderMapper {

    AccessProvider getEntity(Long accessProviderId);

}
