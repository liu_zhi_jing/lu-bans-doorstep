package cn.tedu.luban.demand.webapi.service.impl;

import cn.tedu.luban.commons.exception.LubanServiceException;
import cn.tedu.luban.commons.pojo.demand.dto.RequestOrderDTO;
import cn.tedu.luban.commons.pojo.demand.dto.RequestOrderListDTO;
import cn.tedu.luban.commons.pojo.demand.model.AccessProvider;
import cn.tedu.luban.commons.pojo.demand.model.RequestOrder;
import cn.tedu.luban.commons.pojo.demand.vo.RequestOrderListItemVO;
import cn.tedu.luban.commons.pojo.demand.vo.RequestOrderVO;
import cn.tedu.luban.commons.restful.JsonPage;
import cn.tedu.luban.commons.restful.ResponseCode;
import cn.tedu.luban.commons.utils.PropertiesUtils;
import cn.tedu.luban.demand.service.IRequestOrderService;
import cn.tedu.luban.demand.webapi.mapper.AccessProviderMapper;
import cn.tedu.luban.demand.webapi.mapper.RequestOrderMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import java.math.BigDecimal;
import java.util.List;

@Service
@Slf4j
@DubboService
public class RequestOrderService implements IRequestOrderService {

    @Autowired
    private RequestOrderMapper requestOrderMapper;

    @Autowired
    private AccessProviderMapper accessProviderMapper;

    @Override
    public Long saveRequestOrder(RequestOrderDTO requestOrderDTO) {
        RequestOrder requestOrder = new RequestOrder();
        BeanUtils.copyProperties(requestOrderDTO, requestOrder);
        requestOrder.setRequestOrderNo(requestOrderDTO.getRequestOrderNo());
        requestOrder.setGmtCreate(System.currentTimeMillis());
        requestOrder.setGmtModified(requestOrder.getGmtCreate());
        requestOrder.setStatus(1);
        requestOrder.setCreateUserId(99L);
        requestOrder.setModifiedUserId(99L);
        requestOrder.setCreateUserName("root");
        requestOrder.setModifiedUserName("root");
        requestOrder.setProviderId(6);
        requestOrderMapper.insert(requestOrder);
        return requestOrder.getId();

    }

    @Override
    public JsonPage<RequestOrderListItemVO> queryRequestOrderList(RequestOrderListDTO requestOrderListDTO, Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<RequestOrder> list = requestOrderMapper.queryRequestOrders(requestOrderListDTO);

        List<RequestOrderListItemVO> voList = PropertiesUtils.copyListProperties(list, RequestOrderListItemVO.class);
        for (int i = 0; i < list.size(); i++) {
            RequestOrderListItemVO vo = voList.get(i);
            vo.setCategoryId(list.get(i).getOrderCategoryId());
            vo.setCreateTime(list.get(i).getGmtCreate());
            AccessProvider accessProvider = accessProviderMapper.getEntity(list.get(i).getProviderId() * 1L);
            if (accessProvider != null) {
                // 供应商分润比例
                BigDecimal profitScale = accessProvider.getProfitScale();
                Long workerAmount = getWorkerAmount(list.get(i).getOrderAmount(), profitScale);
                // 订单金额
                vo.setViewOrderAmount(workerAmount);
                vo.setProfitScale(profitScale);

            }
        }

        return JsonPage.restPage(new PageInfo<>(voList));


    }

    @Override
    public RequestOrderVO getRequestOrder(String requestOrderNo) {
        RequestOrder requestOrder = requestOrderMapper.getRequestOrderByOrderNo(requestOrderNo);
        if(requestOrder==null){
            throw new LubanServiceException(ResponseCode.INTERNAL_SERVER_ERROR,"需求单不存在");
        }
        RequestOrderVO requestOrderVO = new RequestOrderVO();
        BeanUtils.copyProperties(requestOrder, requestOrderVO);
        requestOrderVO.setCategoryId(requestOrder.getOrderCategoryId());
        requestOrderVO.setCreateTime(requestOrder.getGmtCreate());
        System.out.println("accessProviderMapper:"+accessProviderMapper);
        AccessProvider accessProvider = accessProviderMapper.getEntity(requestOrder.getProviderId() * 1L);
        if (accessProvider != null) {
            // 供应商分润比例
            BigDecimal profitScale = accessProvider.getProfitScale();
            Long workerAmount = getWorkerAmount(requestOrder.getOrderAmount(), profitScale);
            // 订单金额
            requestOrderVO.setViewOrderAmount(workerAmount);
        }

        return requestOrderVO;
    }

    @Override
    public Object grabOrder(String requestOrderId) {

        return requestOrderMapper.grabOrder(requestOrderId);
    }

    @Override
    public Object cancelGrabOrder(String requestOrderId) {
        return requestOrderMapper.cancelGrabOrder(requestOrderId);
    }

    private Long getWorkerAmount(Long price, BigDecimal scale) {
        if (scale != null) {
            return new BigDecimal(price).multiply(new BigDecimal(100).subtract(scale)).divide(new BigDecimal(100)).longValue();
        }
        //默认20
        return new BigDecimal(price).multiply(new BigDecimal(20)).divide(new BigDecimal(100)).longValue();
    }

}
















