package cn.tedu.luban.commons.pojo.worker.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CategoryVO implements Serializable {
    @ApiModelProperty("品类 Id")
    Long categoryId;
    @ApiModelProperty("品类详情")
    String categoryDetail;

}














