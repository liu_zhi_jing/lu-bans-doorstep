package cn.tedu.luban.commons.pojo.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderInfoVo implements Serializable {

    @ApiModelProperty("订单号")
    String orderNo;

    @ApiModelProperty("创建日期")
    Long createTime;

    @ApiModelProperty("订单状态")
    Integer status;

    @ApiModelProperty("签到日期")
    Long signTime;

    @ApiModelProperty("完成日期")
    Long finishTime;


}
