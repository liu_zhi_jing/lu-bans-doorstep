package cn.tedu.luban.commons.pojo.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AttachInfoVO implements Serializable {
    @ApiModelProperty("地址")
    String url;

}
