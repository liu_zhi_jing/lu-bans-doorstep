package cn.tedu.luban.commons.exception;

import cn.tedu.luban.commons.restful.ResponseCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务异常
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class LubanServiceException extends RuntimeException {

    private ResponseCode responseCode;

    public LubanServiceException(ResponseCode responseCode, String message) {
        super(message);
        setResponseCode(responseCode);
    }

}
