package cn.tedu.luban.commons.pojo.category.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkerCategoryDTO implements Serializable {
    Long userId;
    Long categoryId;
    String categoryDetail;
}
