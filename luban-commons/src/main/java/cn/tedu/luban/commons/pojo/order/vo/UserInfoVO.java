package cn.tedu.luban.commons.pojo.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserInfoVO implements Serializable {

    @ApiModelProperty("用户姓名")
    String userName;

    @ApiModelProperty("用户电话")
    String userPhone;

    @ApiModelProperty("用户地址")
    String userAddress;

}
