package cn.tedu.luban.commons.pojo.account.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountDTO implements Serializable {
    Long userId;
    String userName;
    String userPhone;
}
