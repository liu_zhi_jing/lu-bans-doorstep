package cn.tedu.luban.commons.utils;

import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

public class PropertiesUtils {


    public static <O, I> List<O> copyListProperties(List<I> in, Class<O> clazz) {
        List<O> out = new ArrayList<>();
        try {
            for (I i : in) {
                O o = clazz.newInstance();
                BeanUtils.copyProperties(i,o);
                out.add(o);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return out;

    }


}
