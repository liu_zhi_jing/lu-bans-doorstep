package cn.tedu.luban.commons.pojo.attach.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class AttachUpdateDTO implements Serializable {

    @NotNull(message = "是否是封面不能为空[1:是;0:否;]")
    @ApiModelProperty("是否封面")
    private Integer isCover;

    @NotNull(message = "系统类型")
    @ApiModelProperty("系统类型")
    private Integer businessType;

    @NotNull(message = "业务id不能为空")
    @ApiModelProperty("业务id")
    private Integer businessId;

    @NotNull(message = "id不能为空")
    @ApiModelProperty
    private Integer id;
}
