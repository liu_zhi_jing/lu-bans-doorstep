/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.tedu.luban.commons.pojo.worker.model;


import cn.tedu.luban.commons.pojo.PO;
import io.github.classgraph.json.Id;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = "worker")
public class Worker extends PO implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)")
    /**
     * 主键
     */
    private Long id;

    /**
     * Passport 用户ID
     */
    @Column(name = "user_id", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT 'Passport 用户ID'", updatable = false)
    private Long userId;

    /**
     * 出生日期
     */
    @Column(name = "birthday", columnDefinition = "datetime  COMMENT '出生日期'", updatable = false)
    private Date birthday;

    /**
     * 手机号
     */
    @Column(name = "phone", columnDefinition = "varchar(16) DEFAULT '' COMMENT '手机号'")
    private String phone;

    /**
     * 真实姓名
     */
    @Column(name = "real_name", columnDefinition = "varchar(16) DEFAULT '' COMMENT '真实姓名'")
    private String realName;

    /**
     * 身份证号
     */
    @Column(name = "id_card", columnDefinition = "varchar(16) DEFAULT '' COMMENT '身份证号'")
    private String IdCard;
    /**
     * 账号是否有效状态
     */
    @Column(name = "status", columnDefinition = "tinyint(1)  DEFAULT 0 COMMENT '状态 0:锁定，1:正常'")
    private Integer status;
    //--------新增字段----

    @Column(name = "audit_status",columnDefinition = "tinyint(1)  DEFAULT 0 COMMENT '审核状态'")
    private Integer auditStatus;


    @Column(name = "cert_status",columnDefinition = "tinyint(1)  DEFAULT 0 COMMENT '状态 0:无效，1:通过'")
    private Integer certStatus;


}
