package cn.tedu.luban.commons.pojo;

import javax.persistence.Column;

public class PO {
    @Column(
            name = "create_user_name",
            columnDefinition = "varchar(64)  DEFAULT '' COMMENT '创建人'",
            nullable = false
    )
    private String createUserName;
    @Column(
            name = "create_user_id",
            columnDefinition = "int(11) unsigned  DEFAULT 0 COMMENT '创建人ID'",
            nullable = false
    )
    private Long createUserId;
    @Column(
            name = "modified_user_id",
            columnDefinition = "int(11) unsigned  DEFAULT 0 COMMENT '更新人ID'",
            nullable = false
    )
    private Long modifiedUserId;
    @Column(
            name = "modified_user_name",
            columnDefinition = "varchar(64)  DEFAULT '' COMMENT '更新人'",
            nullable = false
    )
    private String modifiedUserName;
    @Column(
            name = "gmt_modified",
            columnDefinition = "bigint(11)  DEFAULT 0 COMMENT '更新时间'",
            nullable = false
    )
    private Long gmtModified;
    @Column(
            name = "gmt_create",
            columnDefinition = "bigint(11)  DEFAULT 0 COMMENT '创建时间'",
            nullable = false,
            updatable = false
    )
    private Long gmtCreate;

    public PO() {
    }

    public String getCreateUserName() {
        return this.createUserName;
    }

    public Long getCreateUserId() {
        return this.createUserId;
    }

    public Long getModifiedUserId() {
        return this.modifiedUserId;
    }

    public String getModifiedUserName() {
        return this.modifiedUserName;
    }

    public Long getGmtModified() {
        return this.gmtModified;
    }

    public Long getGmtCreate() {
        return this.gmtCreate;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public void setModifiedUserId(Long modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public void setModifiedUserName(String modifiedUserName) {
        this.modifiedUserName = modifiedUserName;
    }

    public void setGmtModified(Long gmtModified) {
        this.gmtModified = gmtModified;
    }

    public void setGmtCreate(Long gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PO)) {
            return false;
        } else {
            PO other = (PO)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                Object this$createUserId = this.getCreateUserId();
                Object other$createUserId = other.getCreateUserId();
                if (this$createUserId == null) {
                    if (other$createUserId != null) {
                        return false;
                    }
                } else if (!this$createUserId.equals(other$createUserId)) {
                    return false;
                }

                Object this$modifiedUserId = this.getModifiedUserId();
                Object other$modifiedUserId = other.getModifiedUserId();
                if (this$modifiedUserId == null) {
                    if (other$modifiedUserId != null) {
                        return false;
                    }
                } else if (!this$modifiedUserId.equals(other$modifiedUserId)) {
                    return false;
                }

                Object this$gmtModified = this.getGmtModified();
                Object other$gmtModified = other.getGmtModified();
                if (this$gmtModified == null) {
                    if (other$gmtModified != null) {
                        return false;
                    }
                } else if (!this$gmtModified.equals(other$gmtModified)) {
                    return false;
                }

                label62: {
                    Object this$gmtCreate = this.getGmtCreate();
                    Object other$gmtCreate = other.getGmtCreate();
                    if (this$gmtCreate == null) {
                        if (other$gmtCreate == null) {
                            break label62;
                        }
                    } else if (this$gmtCreate.equals(other$gmtCreate)) {
                        break label62;
                    }

                    return false;
                }

                label55: {
                    Object this$createUserName = this.getCreateUserName();
                    Object other$createUserName = other.getCreateUserName();
                    if (this$createUserName == null) {
                        if (other$createUserName == null) {
                            break label55;
                        }
                    } else if (this$createUserName.equals(other$createUserName)) {
                        break label55;
                    }

                    return false;
                }

                Object this$modifiedUserName = this.getModifiedUserName();
                Object other$modifiedUserName = other.getModifiedUserName();
                if (this$modifiedUserName == null) {
                    if (other$modifiedUserName != null) {
                        return false;
                    }
                } else if (!this$modifiedUserName.equals(other$modifiedUserName)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof PO;
    }



    public String toString() {
        return "PO(createUserName=" + this.getCreateUserName() + ", createUserId=" + this.getCreateUserId() + ", modifiedUserId=" + this.getModifiedUserId() + ", modifiedUserName=" + this.getModifiedUserName() + ", gmtModified=" + this.getGmtModified() + ", gmtCreate=" + this.getGmtCreate() + ")";
    }
}
