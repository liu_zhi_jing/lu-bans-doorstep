package cn.tedu.luban.commons.pojo.worker.vo;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class IdCardVO implements Serializable {

    boolean valid;

    String realName;

    String cardNo;

}
