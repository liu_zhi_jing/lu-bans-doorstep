package cn.tedu.luban.commons.pojo.demand.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderVO implements Serializable {

    @ApiModelProperty("编号")
    private Long id;

    @ApiModelProperty("需求单编号")
    private String requestOrderNo;

    @ApiModelProperty("订单名称")
    private String orderName;

    @ApiModelProperty("订单类型")
    private Integer categoryId;

    @ApiModelProperty("单价")
    private Long orderAmount;

    @ApiModelProperty("展示价格")
    private Long viewOrderAmount;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("手机号")
    private String userPhone;

    @ApiModelProperty("上门详细地址")
    private String address;

    @ApiModelProperty("创建时间")
    private Long createTime;

}
