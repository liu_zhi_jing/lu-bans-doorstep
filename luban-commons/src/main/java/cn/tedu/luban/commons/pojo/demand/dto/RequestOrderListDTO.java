package cn.tedu.luban.commons.pojo.demand.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderListDTO implements Serializable {

    @ApiModelProperty("订单类型Id")
    private List<Integer> orderCategoryIds;

    @ApiModelProperty("地区Id")
    private List<Integer> areaIds;
}
