package cn.tedu.luban.commons.pojo.worker.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Area implements Serializable {

    /**
     * 主键 Id
     */
    private Long id;

    /**
     * 父级 ID
     */
    private Long parentId;

    /**
     * 地址编码
     */
    private String adCode;

    /**
     * 城市编码
     */
    private String cityCode;

    /**
     * 省/直辖市/市/区名称
     */
    private String name;
    /**
     * 1:省 2:市  3:区
     */
    private Integer depth;

    /**
     * 0:未删除
     * 1:已删除
     */
    private Integer isDelete;
    /**
     * 是否是直辖市
     * 1:直辖市
     * 0:其他
     */
    private Integer isMunicipality;


}
