package cn.tedu.luban.commons.pojo.area.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkerAreaDTO implements Serializable {
    Long userId;
    Long areaId;
    String areaDetail;
}
