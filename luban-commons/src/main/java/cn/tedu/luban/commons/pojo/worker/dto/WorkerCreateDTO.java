package cn.tedu.luban.commons.pojo.worker.dto;

import cn.tedu.luban.commons.pojo.area.dto.WorkerAreaDTO;
import cn.tedu.luban.commons.pojo.category.dto.WorkerCategoryDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkerCreateDTO implements Serializable {
    /**
     * 主键 id
     */
    @ApiModelProperty("用户 ID")
    Long userId;

    /**
     * 真实姓名
     */
    @ApiModelProperty("真实姓名")
    String realName;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    String phone;

    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    String idCard;

    /**
     * 身份证图片地址
     */
    @ApiModelProperty("图片地址")
    List<IdCardDTO> attachList;

    /**
     * 区域 id
     */
    @ApiModelProperty("区域信息")
    List<WorkerAreaDTO>  workerAreaList;

    /**
     * 区域 id
     */
    @ApiModelProperty("品类信息")
    List<WorkerCategoryDTO>  workerCategoryList;
}
