package cn.tedu.luban.commons.enums.account;

public enum StatusRecord {
    ENABLE(1, "启用"),
    DISABLE(0, "禁用");

    private int status;
    private String desc;

    private StatusRecord(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return this.status;
    }

    public String getDesc() {
        return this.desc;
    }
}