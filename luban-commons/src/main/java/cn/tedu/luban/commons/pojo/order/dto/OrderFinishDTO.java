package cn.tedu.luban.commons.pojo.order.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderFinishDTO implements Serializable {
    String orderNo;
}
