package cn.tedu.luban.commons.pojo.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderInfoVO implements Serializable {

    @ApiModelProperty("订单号")
    String requestOrderNo;

    @ApiModelProperty("需求单类型")
    String requestOrderCategoryName;

    @ApiModelProperty("单价")
    Long requestOrderPrice;

    @ApiModelProperty("预约时间")
    Long serviceTime;

}
