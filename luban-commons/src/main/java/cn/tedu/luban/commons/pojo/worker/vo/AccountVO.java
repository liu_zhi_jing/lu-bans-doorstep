package cn.tedu.luban.commons.pojo.worker.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountVO implements Serializable {

    @ApiModelProperty(value = "金额")
    Long totalAmount;

}














