package cn.tedu.luban.commons.pojo.worker.query;



import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class WorkerQuery implements Serializable {
    @ApiModelProperty("师傅编号")
    private String userId;

    @ApiModelProperty("师傅姓名")
    private String realName;

    @ApiModelProperty("师傅手机号")
    private String phone;

    @ApiModelProperty("身份证号码")
    private String IdCard;

    @ApiModelProperty("账号状态 0:锁定  1:正常")
    private Integer status;

    @ApiModelProperty("审核状态 0:驳回  1:通过   2:未审核")
    private List<Integer> auditStatus;

    @ApiModelProperty("开始日期")
    private Long startDate;

    @ApiModelProperty("结束日期")
    private Long endDate;



}
