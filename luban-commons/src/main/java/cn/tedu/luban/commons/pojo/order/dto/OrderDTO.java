package cn.tedu.luban.commons.pojo.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderDTO implements Serializable {

    @ApiModelProperty("请求单编号")
    String requestOrderNo;

    @ApiModelProperty("需求单类型id")
    Integer orderCategoryId;

    @ApiModelProperty("需求单类型")
    String orderCategoryName;

    @ApiModelProperty("需求单价格")
    Long requestOrderPrice;

    @ApiModelProperty("需求单原始价格")
    Long requestOrderRawPrice;

    @ApiModelProperty("用户ID")
    Long userId;

    @ApiModelProperty("用户姓名")
    String userName;

    @ApiModelProperty("用户手机")
    String userPhone;

    @ApiModelProperty("用户地址")
    String userAddress;

    @ApiModelProperty("服务时间")
    Long serviceTime;

    @ApiModelProperty("师傅姓名")
    String workerName;

    @ApiModelProperty("师傅手机")
    String workerPhone;

    @ApiModelProperty("分润比例")
    Long profitScale;

    String orderNo;

    Integer status;

    Long id;
}
