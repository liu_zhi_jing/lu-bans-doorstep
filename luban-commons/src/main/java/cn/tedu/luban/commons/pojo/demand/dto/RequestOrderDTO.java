package cn.tedu.luban.commons.pojo.demand.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderDTO implements Serializable {

    @ApiModelProperty("用户下单姓名")
    String userName;

    @ApiModelProperty("用户手机号")
    String userPhone;

    @ApiModelProperty("订单名")
    String orderName;

    @ApiModelProperty("需求单编号")
    String requestOrderNo;

    @ApiModelProperty("区域id")
    Integer areaId;

    @ApiModelProperty("订单类型")
    Integer orderCategoryId;

    @ApiModelProperty("订单单价")
    Long orderAmount;

    @ApiModelProperty("最晚交付时间")
    Long serviceTime;

    @ApiModelProperty("上门详细地址")
    String address;


}
