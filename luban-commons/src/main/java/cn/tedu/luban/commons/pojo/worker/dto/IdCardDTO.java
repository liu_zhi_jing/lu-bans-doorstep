package cn.tedu.luban.commons.pojo.worker.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class IdCardDTO implements Serializable {

    @ApiModelProperty("身份证图片地址")
    private String URL;

    @ApiModelProperty("文件id")
    private Integer id;

    @ApiModelProperty("图片类型 type 1:身份证正面 type 2:身份证反面")
    private int type;
}
