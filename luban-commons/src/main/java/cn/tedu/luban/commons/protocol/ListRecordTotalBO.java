package cn.tedu.luban.commons.protocol;

import java.util.List;

public class ListRecordTotalBO<T> {
    private List<T> list;
    private Long total;

    public ListRecordTotalBO(List<T> list, Long total) {
        this.list = list;
        this.total = total;
    }

    public List<T> getList() {
        return this.list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Long getTotal() {
        return this.total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
