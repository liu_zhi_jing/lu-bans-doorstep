package cn.tedu.luban.commons.pojo.attach.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class AttachDTO implements Serializable {

    @ApiModelProperty("系统类型")
    private Integer businessType;

    @ApiModelProperty("业务ID")
    private Integer businessId;
}
