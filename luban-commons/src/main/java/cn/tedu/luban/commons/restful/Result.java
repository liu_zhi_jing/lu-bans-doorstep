/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.tedu.luban.commons.restful;

import cn.tedu.luban.commons.exception.LubanServiceException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@ApiModel("全局返回包装实体")
@AllArgsConstructor
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 2463268310118367610L;

    @ApiModelProperty(value = "0 代表成功, 其他均为失败")
    private String code;

    @ApiModelProperty(value = "描述")
    private String message;

    @ApiModelProperty(value = "真正实体")
    private T data;

    private static Result<Void> ok = new Result<>();

    private Result() {
        this.code = "0";
        this.message = "success";
    }

    public static Result<Void> success() {
        return new Result<>();
    }

    public Result(T data) {
            this.code = "0";
            this.data = data;
    }

    private Result(String code, String message) {
        this.code = code;
        this.message = message;
    }


    @SuppressWarnings("rawtypes")
    public static Result fail() {
        String code = "-1";
        String msg = "system error";
        return new Result(code, msg);
    }

    @SuppressWarnings("rawtypes")
    public static Result fail(LubanServiceException e) {
        return new Result(ResponseCode.INTERNAL_SERVER_ERROR.getValue()+"", e.getMessage());
    }



    @SuppressWarnings("rawtypes")
    public static Result fail(String code, String message) {
        return new Result(code, message);
    }

    public boolean isSuccess() {
        return this.code.equals("0");
    }





}
