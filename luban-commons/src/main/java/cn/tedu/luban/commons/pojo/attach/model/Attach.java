package cn.tedu.luban.commons.pojo.attach.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Attach implements Serializable {

    private Integer id;

    private String fileUuid;

    private String clientFileName;

    private Integer downloadTimes;

    private Long contentLength;

    private Integer contentType;

    private Integer isCover;

    private Integer width;

    private Integer height;

    private Integer businessType;

    private Integer businessId;

    private Integer status;

    private String remark;

    private Long createUserId;

    private Long gmtCreate;

    private Long modifiedUserId;

    private Long gmtModified;

    private String createUserName;

    private String modifiedUserName;

}
