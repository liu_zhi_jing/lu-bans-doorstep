package cn.tedu.luban.commons.pojo.worker.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AreaVO implements Serializable {


    @ApiModelProperty(value = "区域列表 ID")
    Long areaId;

    @ApiModelProperty(value = "区域详情")
    String areaDetail;
}














