package cn.tedu.luban.commons.pojo.worker.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 师傅详情中的 账户信息
 */
@Data
public class AccountInfoVO implements Serializable {

    /**
     * 账户累计收入
     */
    @ApiModelProperty("账户累计收入")
    private Integer accountIncome;


    /**
     *  账户余额
     */
    @ApiModelProperty("账户余额")
    private Integer accountBalance;


    /**
     * 待收金额
     */
    @ApiModelProperty("待收金额")
    private Integer Amount;

    /**
     * 账户状态
     */
    @ApiModelProperty("账户状态")
    private String accountStatus;
}
