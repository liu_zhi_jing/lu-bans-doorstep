package cn.tedu.luban.commons.pojo.worker.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 师傅详情中的 银行卡信息
 */
@Data
public class BankInfoVO implements Serializable {

    /**
     * 绑定状态
     */
    @ApiModelProperty("绑定状态")
    private String bindingState;

    /**
     * 开户银行
     */
    @ApiModelProperty("开户银行")
    private String Bank;

    /**
     * 银行卡号
     */
    @ApiModelProperty("银行卡号")
    private String BankNo;

    /**
     * 账户状态
     */
    @ApiModelProperty("账户状态")
    private String accountStatus;

    /**
     * 持卡人姓名
     */
    @ApiModelProperty("持卡人姓名")
    private String bankName;

    /**
     * 预留手机号
     */
    private String bankPhone;
}
