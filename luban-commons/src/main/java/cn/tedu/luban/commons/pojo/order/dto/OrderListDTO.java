package cn.tedu.luban.commons.pojo.order.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderListDTO implements Serializable {

    List<Integer> status;

    Long userId;
}
