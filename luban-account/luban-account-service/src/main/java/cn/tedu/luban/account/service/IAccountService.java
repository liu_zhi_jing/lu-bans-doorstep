package cn.tedu.luban.account.service;

import cn.tedu.luban.commons.pojo.account.dto.AccountDTO;
import cn.tedu.luban.commons.pojo.account.dto.PaymentDTO;
import cn.tedu.luban.commons.pojo.account.model.Account;

public interface IAccountService {

    Account getAccountByUserId(Long userId);

    Long create(AccountDTO accountDTO);

    Long mockPayment(PaymentDTO paymentDTO);
}
