package cn.tedu.luban.account.webapi.controller;

import cn.tedu.luban.account.service.IAccountService;
import cn.tedu.luban.commons.exception.LubanServiceException;
import cn.tedu.luban.commons.pojo.account.dto.AccountDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@Api(value = "account", tags = "账户接口")
@Slf4j
@RestController
@RequestMapping("account")
public class AccountController {

    @Autowired
    private IAccountService accountService;

    @PostMapping(value = "save",produces = "application/json")
    @ApiOperation("创建账户")
    public Long saveAccount(AccountDTO accountDTO) throws LubanServiceException {
        return accountService.create(accountDTO);
    }
}
