package cn.tedu.luban.account.webapi.service.impl;

import cn.tedu.luban.account.service.IAccountService;
import cn.tedu.luban.account.webapi.mapper.AccountMapper;
import cn.tedu.luban.account.webapi.utils.SecurityContext;
import cn.tedu.luban.commons.enums.account.StatusRecord;
import cn.tedu.luban.commons.pojo.account.dto.AccountDTO;
import cn.tedu.luban.commons.pojo.account.dto.PaymentDTO;
import cn.tedu.luban.commons.pojo.account.model.Account;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Service
@DubboService
public class AccountServiceImpl implements IAccountService {
    @Autowired
    private AccountMapper accountMapper;
    @Override
    public Account getAccountByUserId(Long userId) {
        Account account=accountMapper.getAccountByUserId(userId);
        return account;
    }


    @Override
    public Long create(AccountDTO accountDTO) {

        Account account = new Account();
        BeanUtils.copyProperties(accountDTO, account);
        Long userId= SecurityContext.getUserId();
        account.setGmtCreate(System.currentTimeMillis());
        account.setGmtModified(System.currentTimeMillis());
        account.setCreateUserId(userId);
        account.setModifiedUserId(userId);
        account.setCreateUserName("mock1");
        account.setModifiedUserName("mock1");
        account.setStatus(StatusRecord.ENABLE.getStatus());
        account.setSettlingAmount(0L);
        account.setTotalAmount(0L);
        accountMapper.insert(account);
        return account.getId();
    }

    @Override
    public Long mockPayment(PaymentDTO paymentDTO) {
        Account account = new Account();
        BeanUtils.copyProperties(paymentDTO, account);
        Long userId=SecurityContext.getUserId();
        account.setGmtCreate(System.currentTimeMillis());
        account.setGmtModified(System.currentTimeMillis());
        account.setCreateUserId(userId);
        account.setModifiedUserId(userId);
        account.setCreateUserName( "mock1");
        account.setModifiedUserName( "mock1");
        account.setStatus(StatusRecord.ENABLE.getStatus());
        return accountMapper.updateAmount( account);
    }

}
