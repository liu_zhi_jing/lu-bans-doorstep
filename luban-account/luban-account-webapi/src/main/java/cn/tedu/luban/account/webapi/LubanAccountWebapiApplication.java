package cn.tedu.luban.account.webapi;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class LubanAccountWebapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LubanAccountWebapiApplication.class, args);
    }

}
