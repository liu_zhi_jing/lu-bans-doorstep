package cn.tedu.luban.account.webapi.mapper;

import cn.tedu.luban.commons.pojo.account.model.Account;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountMapper {


    Account getAccountByUserId(Long userId);

    Long insert(Account account);

    Long updateAmount(Account param);


}
