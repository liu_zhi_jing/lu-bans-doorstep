package cn.tedu.luban.attach.webapi.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
// MyBatis框架要求扫描指定的包,包中的接口会生成实现类
@MapperScan("cn.tedu.luban.attach.webapi.mapper")
public class MyBatisConfiguration {
}



