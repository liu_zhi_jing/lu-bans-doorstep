package cn.tedu.luban.attach.webapi.mapper;

import cn.tedu.luban.commons.pojo.attach.dto.AttachDTO;
import cn.tedu.luban.commons.pojo.attach.dto.AttachUpdateDTO;
import cn.tedu.luban.commons.pojo.attach.model.Attach;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttachMapper {

    List<Attach> getAttachInfo(AttachDTO attachDTO);

    int batchUpdateAttachByIdList(List<AttachUpdateDTO> attachUpdateDTOList);
}
