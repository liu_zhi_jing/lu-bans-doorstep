package cn.tedu.luban.attach.webapi.service.impl;

import cn.tedu.luban.attach.service.IAttachService;
import cn.tedu.luban.attach.webapi.mapper.AttachMapper;
import cn.tedu.luban.commons.pojo.attach.dto.AttachDTO;
import cn.tedu.luban.commons.pojo.attach.dto.AttachUpdateDTO;
import cn.tedu.luban.commons.pojo.attach.model.Attach;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@DubboService
public class AttachServiceImpl implements IAttachService {

    @Autowired
    private AttachMapper attachMapper;

    @Override
    public List<Attach> getAttachInfo(AttachDTO attachDTO) {
        List<Attach> attachList=attachMapper.getAttachInfo(attachDTO);
        return attachList;
    }

    @Override
    public int batchUpdateAttachByIdList(List<AttachUpdateDTO> attachUpdateDTOList) {

        return attachMapper.batchUpdateAttachByIdList(attachUpdateDTOList);
    }
}
