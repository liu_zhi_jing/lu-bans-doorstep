package cn.tedu.luban.attach.service;


import cn.tedu.luban.commons.pojo.attach.dto.AttachDTO;
import cn.tedu.luban.commons.pojo.attach.dto.AttachUpdateDTO;
import cn.tedu.luban.commons.pojo.attach.model.Attach;

import java.util.List;

public interface IAttachService {

    List<Attach> getAttachInfo(AttachDTO attachDTO);

    int batchUpdateAttachByIdList(List<AttachUpdateDTO> attachUpdateDTOList);
}
