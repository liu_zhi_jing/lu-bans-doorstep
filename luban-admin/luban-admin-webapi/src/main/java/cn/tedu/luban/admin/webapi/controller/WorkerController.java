package cn.tedu.luban.admin.webapi.controller;


import cn.tedu.luban.admin.service.WorkerService;
import cn.tedu.luban.commons.enums.worker.AuditStatusEnum;

import cn.tedu.luban.commons.pojo.worker.query.WorkerQuery;
import cn.tedu.luban.commons.pojo.worker.vo.WorkerVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;

@RestController
@RequestMapping("admin/worker")
@Api(value = "worker",tags = "师傅服务")
public class WorkerController {
    @Resource
    private WorkerService workerService;


//    @PostMapping(value = "search")
//    @ApiOperation(value = "师傅列表")
//    public Result<PagerResult<WorkerVO>> queryWorkers(WorkerQuery workerQuery) {
//        workerQuery.setAuditStatus(Collections.singletonList(AuditStatusEnum.APPROVED.getValue()));
//        PagerResult<WorkerVO> pagerResult = workerService.queryWorker(workerQuery);
//        return Result.success(pagerResult);
//    }

}
