package cn.tedu.luban.admin.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LubanAdminWebapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LubanAdminWebapiApplication.class, args);
    }

}
