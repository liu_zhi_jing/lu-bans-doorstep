package cn.tedu.luban.admin.webapi.service.impl;



import cn.tedu.luban.admin.webapi.mapper.WorkerAreaMapper;
import cn.tedu.luban.admin.webapi.mapper.WorkerCategoryMapper;
import cn.tedu.luban.admin.webapi.mapper.WorkerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkerServiceImpl  {

    @Autowired
    private WorkerMapper workerMapper;

    @Autowired
    private WorkerAreaMapper workerAreaMapper;

    @Autowired
    private WorkerCategoryMapper workerCategoryMapper;


//    public PagerResult<WorkerVO> queryWorker(WorkerQuery workerQuery) {
//        PageHelper.startPage(workerQuery.getPageNo(), workerQuery.getPageSize());
//        List<WorkerVO> workerList = workerMapper.queryWorkers(workerQuery);
//        for (WorkerVO workerVO : workerList) {
//            appendServiceInfo(workerVO);
//        }
//        return PageConverter.restPage(new PageInfo<>(workerList));
//    }
//
//    private void appendServiceInfo(WorkerVO workerVO) {
//        List<WorkerAreaBO> workerArea = workerAreaMapper.getWorkerArea(workerVO.getUserId());
//        if (workerArea != null) {
//            List<String> collect = workerArea.stream().map(WorkerAreaBO::getAreaDetail).collect(Collectors.toList());
//            workerVO.setAreaDetails(collect);
//        }
//        List<WorkerCategoryBO> workerCategory = workerCategoryMapper.getWorkerCategory(workerVO.getUserId());
//        if (workerCategory != null) {
//            List<String> collect = workerCategory.stream().map(WorkerCategoryBO::getCategoryDetail).collect(Collectors.toList());
//            workerVO.setCategoryDetails(collect);
//        }
//    }

}
