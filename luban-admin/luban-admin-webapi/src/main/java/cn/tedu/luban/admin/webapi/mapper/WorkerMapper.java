package cn.tedu.luban.admin.webapi.mapper;

import cn.tedu.luban.commons.pojo.worker.bo.WorkerBO;
import cn.tedu.luban.commons.pojo.worker.query.WorkerQuery;
import cn.tedu.luban.commons.pojo.worker.vo.WorkerVO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkerMapper {

    Long getWorkerCount(WorkerQuery workerQuery);

    List<WorkerVO> queryWorkers(WorkerQuery workerQuery);

}